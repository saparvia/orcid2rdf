import sys
import urllib.request
import xml.etree.ElementTree as ET
import hashlib

from rdflib import Graph, Literal, URIRef, BNode, Namespace
from rdflib.namespace import FOAF, RDF, DC, DCTERMS

CV = Namespace('http://rdfs.org/resume-rdf/cv.rdfs#')
BIO = Namespace('http://vocab.org/bio/')

META = Namespace('http://pafcu.fi/ns/meta/')
SCIPUB = Namespace('http://pafcu.fi/ns/scipub/')

def triple_uri(triple):
	return URIRef('triple:sha1:%s'%hashlib.sha1(bytes(str(triple),'utf-8')).hexdigest())

ns = { 'o': 'http://www.orcid.org/ns/orcid' } # Needs to be included when working with xpath

def format_date(element):
	date = ''
	year = element.find('o:year', ns)
	if year == None:
		return date
	date += '%04d'%int(year.text)

	month = element.find('o:month', ns)
	if month == None:
		return date
	date += '-%02d'%int(month.text)

	day = element.find('o:day', ns)
	if day == None:
		return date
	date += '-%02d'%int(day.text)

	return date

def get_profile(orcid):
	g = Graph()

	url = 'https://pub.orcid.org/v1.2/%s/orcid-profile'%orcid
	xml = ET.parse(urllib.request.urlopen(url))

	profile = xml.find('o:orcid-profile', ns)

	orcid_uri = URIRef(profile.find('o:orcid-identifier/o:uri', ns).text)

	g.add((orcid_uri, RDF.type, FOAF.Person))

	bio = profile.find('o:orcid-bio/o:biography', ns)
	if bio != None:
		g.add((orcid_uri, BIO.olb, Literal(bio.text)))

	details = profile.find('o:orcid-bio/o:personal-details', ns)

	g.add((orcid_uri, FOAF.familyName, Literal(details.find('o:family-name', ns).text)))
	g.add((orcid_uri, FOAF.givenName, Literal(details.find('o:given-names', ns).text)))

	for email in profile.findall('o:orcid-bio/o:contact-details/o:email', ns):
		g.add((orcid_uri, FOAF.mbox, URIRef('mailto:%s'%email.text)))
	
	for work in profile.findall('o:orcid-activities/o:orcid-works/o:orcid-work', ns):
		puburi = None
		# TODO: Support other identifiers than DOI, add owl:sameAs to link them all together
		for ext_id in work.findall('o:work-external-identifiers/o:work-external-identifier', ns):
			if ext_id.find('o:work-external-identifier-type', ns).text == 'doi':
				# DOIs are case insensitve, while URIs are not, so normalize to lower case
				puburi = 'http://dx.doi.org/'+ext_id.find('o:work-external-identifier-id', ns).text.lower()

		if puburi:
			puburi = URIRef(puburi)
		else:
			raise Exception("No DOI found") 

		for i, contributor in enumerate(work.findall('o:work-contributors/o:contributor', ns)):
			role = contributor.find('contributor-attributes/o:contributor-role', ns)
			if not role or role.text == 'author': # Assume author if nothing else is said
				author = BNode()
				g.add((author, FOAF.name, Literal(contributor.find('o:credit-name', ns).text)))
				g.add((puburi, DC.contributor, author))
				g.add((puburi, SCIPUB['author%d'%(i+1)], author)) # RDF is not good for preserving order, so this hackish way is required

		for citation in work.findall('o:work-citation', ns):
			if citation.find('o:work-citation-type', ns).text == 'bibtex':
				g.add((puburi, SCIPUB.bibtex, Literal(citation.find('o:citation', ns).text)))

		title = work.find('o:work-title/o:title', ns).text
		g.add((puburi, DC.title, Literal(title)))

		work_type = work.find('o:work-type', ns)
		if work_type.text == 'journal-article':
			g.add((puburi, RDF.type, SCIPUB.PeerReviewedJournalArticle))

		journal_title = Literal(work.find('o:journal-title', ns).text)
		journal = BNode()
		g.add((journal, DC.title, journal_title))
		g.add((puburi, DCTERMS.isPartOf, journal))

		g.add((puburi, DCTERMS.issued, Literal(format_date(work.find('o:publication-date', ns)))))
		
		g.add((puburi, DC.identifier, puburi))

		g.add((puburi, DC.contributor, orcid_uri))

	for affiliation in profile.findall('o:orcid-activities/o:affiliations/o:affiliation', ns):
		typ = affiliation.find('o:type', ns).text
		workexp = BNode()

		if typ == 'employment':
			g.add((orcid_uri, CV.hasWorkHistory, workexp))
			g.add((workexp, RDF.type, CV.WorkHistory))
		elif typ == 'education':
			g.add((orcid_uri, CV.hasEducation, workexp))
			g.add((workexp, RDF.type, CV.Education))

		if typ == 'employment':
			start_type = CV.startDate
			end_type = CV.endDate
		elif typ == 'education':
			start_type = CV.eduStartDate
			end_type = CV.eduGradDate

		start = affiliation.find('o:start-date', ns)
		end = affiliation.find('o:end-date', ns)
		if start != None:
			g.add((workexp, start_type, Literal(format_date(start))))

		if end != None:
			g.add((workexp, end_type, Literal(format_date(end))))
		
		title = affiliation.find('o:role-title', ns)
		if title != None:
			if typ == 'employment':
				g.add((workexp, CV.jobTitle, Literal(title.text)))
			elif typ == 'education':
				# TODO: Bug, this should be an EduDegree, not a literal
				g.add((workexp, CV.degreeType, Literal(title.text)))

		org = affiliation.find('o:organization', ns)
		if org != None:
			company = BNode()
			name = org.find('o:name', ns).text
			g.add((company, FOAF.name, Literal(name)))
			if typ == 'employment':
				g.add((workexp, CV.employedIn, company))
			elif typ == 'education':
				g.add((workexp, CV.studiedIn, company))

	for triple in g:
		g.add((triple_uri(triple), META.sourceDocument, URIRef(url)))
		g.add((triple_uri(triple), META.sourceService, URIRef('https://orcid.org/')))

	return g
